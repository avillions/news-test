<?php declare(strict_types = 1);

namespace ApiBundle\Controller;

use ApiBundle\Entity\ApiResponse;
use ApiBundle\Entity\Category;
use ApiBundle\Entity\PostData;
use ApiBundle\Form\PostType;
use ApiBundle\Manager\PostManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use ApiBundle\Entity\Post;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class PostController
 * @package ApiBundle\Controller
 */
class PostController extends AbstractController
{
    /**
     * List posts.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Return posts.",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="success", type="boolean"),
     *         @SWG\Property(property="data", type="array", @SWG\Items(ref=@Model(type=Post::class, groups={"api"}))),
     *     )
     * )
     *
     * @SWG\Tag(name="post")
     *
     * @return Response
     * @throws HttpException
     */
    public function getAction()
    {
        if (!$result = $this->getDoctrine()->getRepository(Post::class)->findAll()) {
            throw new HttpException(
                Response::HTTP_NOT_FOUND,
                'There are no posts exist'
            );
        }

        return $this->response(new ApiResponse($result));
    }

    /**
     * Create post.
     *
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     type="json",
     *     description="Post data.",
     *     required=false,
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="title", type="string"),
     *         @SWG\Property(property="text", type="string"),
     *         @SWG\Property(property="categoryId", type="string")
     *     )
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Return created post.",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="success", type="boolean"),
     *         @SWG\Property(property="data", type="object", ref=@Model(type=Post::class, groups={"api"})),
     *     )
     * )
     *
     * @SWG\Tag(name="post")
     *
     * @param Request $request
     * @return Response
     */
    public function postAction(Request $request)
    {
        /** @var PostData $data */
        $data = $this->getFormData(PostType::class, $request);

        $category = $this->getCategory($data->getCategoryId());

        $post = $this->getManager()->create($data, $category);

        return $this->response(new ApiResponse($post, $request));
    }

    /**
     * Update post.
     *
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     type="json",
     *     description="Post data.",
     *     required=false,
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="title", type="string"),
     *         @SWG\Property(property="text", type="string"),
     *         @SWG\Property(property="categoryId", type="string")
     *     )
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Return updated post.",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="success", type="boolean"),
     *         @SWG\Property(property="data", type="object", ref=@Model(type=Post::class, groups={"api"})),
     *     )
     * )
     *
     * @SWG\Tag(name="post")
     *
     * @param Request $request
     * @param int     $id
     * @return Response
     */
    public function putAction(Request $request, int $id)
    {
        $data = $this->getFormData(PostType::class, $request, ['id' => $id]);

        $category = $this->getCategory($data->getCategoryId());

        $post = $this->getManager()->update($data, $category);

        return $this->response(new ApiResponse($post, $request));
    }

    /**
     * Delete post.
     *
     * @SWG\Parameter(name="id", in="path", type="integer", description="Post id.")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Return delete post result.",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="success", type="boolean"),
     *     )
     * )
     *
     * @SWG\Tag(name="post")
     *
     * @param int $id
     * @return null|object
     * @throws \HttpException
     */
    public function deleteAction(int $id)
    {
        $postRepository = $this->getManager()->getRepository();

        /** @var Post $post */
        if (!$post = $postRepository->find($id)) {
            throw new \HttpException(
                Response::HTTP_BAD_REQUEST,
                sprintf('Post #%s not found.', $id)
            );
        }

        $this->getManager()->delete($post);

        return $this->response(new ApiResponse());
    }

    /**
     * @param int $id
     * @return Category
     * @throws \HttpException
     */
    private function getCategory(int $id): Category
    {
        $categoryRepository = $this->get('api.manager.category')->getRepository();

        /** @var Category $category  */
        if (!$category = $categoryRepository->find($id)) {
            throw new \HttpException(
                Response::HTTP_NOT_FOUND,
                sprintf('Category #%s not found.', $id)
            );
        }

        return $category;
    }

    /**
     * @return PostManager
     */
    private function getManager(): PostManager
    {
        return $this->get('api.manager.post');
    }
}
