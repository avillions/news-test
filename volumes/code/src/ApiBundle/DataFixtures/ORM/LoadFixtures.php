<?php declare(strict_types=1);

namespace ApiBundle\DataFixtures\ORM;

use ApiBundle\Entity\Post;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use ApiBundle\Entity\Category;

/**
 * Class LoadFixtures
 * @package ApiBundle\DataFixtures\ORM
 */
class LoadFixtures implements ORMFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $categories = [];

        // Create 10 categories! Bam!
        for ($i = 1; $i <= 10; $i++) {
            $categories[$i] = (new Category())
                ->setName(sprintf('Category #%s', $i))
            ;

            $manager->persist($categories[$i]);
        }

        $manager->flush();

        // Create 20 posts! Dam!
        for ($i = 1; $i <= 20; $i++) {
            $category = $categories[array_rand($categories)];

            $post = (new Post())
                ->setTitle(sprintf('Post #%s', $i))
                ->setText(sprintf('Post text #%s', $i))
                ->setCategory($category)
            ;

            $manager->persist($post);
        }

        $manager->flush();
    }
}