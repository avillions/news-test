<?php declare(strict_types = 1);

namespace ApiBundle\Entity;

use JMS\Serializer\Annotation as JMS;

/**
 * Class ApiResponse
 * @package ApiBundle\Entity
 *
 * @JMS\ExclusionPolicy("ALL")
 */
class ApiResponse
{
    const JMS_DEFAULT_GROUP = 'api';

    const JMS_DEBUG_GROUP = 'debug';

    /**
     * @var bool
     *
     * @JMS\Expose
     * @JMS\Groups({"api"})
     */
    private $success = true;

    /**
     * @var int
     *
     * @JMS\Expose
     * @JMS\Groups({"api"})
     */
    private $errorCode;

    /**
     * @var string
     *
     * @JMS\Expose
     * @JMS\Groups({"api"})
     */
    private $errorMessage;

    /**
     * @var string
     *
     * @JMS\Expose
     * @JMS\Groups({"debug"})
     */
    private $errorFile;

    /**
     * @var int
     *
     * @JMS\Expose
     * @JMS\Groups({"debug"})
     */
    private $errorLine;

    /**
     * @var string
     *
     * @JMS\Expose
     * @JMS\Groups({"debug"})
     */
    private $errorTrace;

    /**
     * @var array|object
     *
     * @JMS\Expose
     * @JMS\Groups({"api"})
     */
    private $parameters;

    /**
     * @var array|object
     *
     * @JMS\Expose
     * @JMS\Groups({"api"})
     */
    private $data;

    /**
     * ApiResponse constructor.
     * @param null     $data
     * @param null     $parameters
     */
    public function __construct($data = null, $parameters = null)
    {
        $this->parameters = $parameters;
        $this->data = $data;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @param bool $success
     * @return ApiResponse
     */
    public function setSuccess(bool $success): self
    {
        $this->success = $success;

        return $this;
    }

    /**
     * @return int
     */
    public function getErrorCode(): ?int
    {
        return $this->errorCode;
    }

    /**
     * @param int $errorCode
     * @return ApiResponse
     */
    public function setErrorCode(int $errorCode): self
    {
        $this->errorCode = $errorCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getErrorMessage(): ?string
    {
        return $this->errorMessage;
    }

    /**
     * @param string $errorMessage
     * @return ApiResponse
     */
    public function setErrorMessage(string $errorMessage): self
    {
        $this->errorMessage = $errorMessage;

        return $this;
    }

    /**
     * @return string
     */
    public function getErrorFile(): ?string
    {
        return $this->errorFile;
    }

    /**
     * @param string $errorFile
     * @return ApiResponse
     */
    public function setErrorFile(string $errorFile): self
    {
        $this->errorFile = $errorFile;

        return $this;
    }

    /**
     * @return int
     */
    public function getErrorLine(): ?int
    {
        return $this->errorLine;
    }

    /**
     * @param int $errorLine
     * @return ApiResponse
     */
    public function setErrorLine(int $errorLine): self
    {
        $this->errorLine = $errorLine;

        return $this;
    }

    /**
     * @return string
     */
    public function getErrorTrace(): ?string
    {
        return $this->errorTrace;
    }

    /**
     * @param string|null $errorTrace
     * @return ApiResponse
     */
    public function setErrorTrace(string $errorTrace = null): self
    {
        $this->errorTrace = $errorTrace;

        return $this;
    }

    /**
     * @return array|object
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * @param array|object $parameters
     * @return ApiResponse
     */
    public function setParameters($parameters): self
    {
        $this->parameters = $parameters;

        return $this;
    }

    /**
     * @return array|object
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param array|object $data
     * @return ApiResponse
     */
    public function setData($data): self
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @param \Exception $exception
     * @return ApiResponse
     */
    public static function createByException(\Exception $exception): self
    {
        $result =  (new self())
            ->setSuccess(false)
            ->setErrorCode($exception->getCode())
            ->setErrorMessage($exception->getMessage())
            ->setErrorFile($exception->getFile())
            ->setErrorLine($exception->getLine())
            ->setErrorTrace($exception->getTraceAsString())
        ;

        return $result;
    }
}
