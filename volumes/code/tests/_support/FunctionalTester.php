<?php

use Symfony\Bundle\FrameworkBundle\Routing\Router;

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = null)
 *
 * @SuppressWarnings(PHPMD)
 */
class FunctionalTester extends \Codeception\Actor
{
    use _generated\FunctionalTesterActions;

    /**
     * @param string $name
     * @param array  $parameters
     * @param int    $referenceType
     * @return mixed
     */
    public function generateUrl(
        string $name,
        array $parameters = [],
        int $referenceType = Router::ABSOLUTE_PATH
    ) {
        /** @var Router $router */
        $router = $this->grabService('router');

        return $router->generate($name, $parameters, $referenceType);
    }
}
