<?php declare(strict_types = 1);

use Codeception\Util\HttpCode;

/**
 * Class CategoriesCest
 */
class CategoriesCest
{
    /**
     * @param FunctionalTester $I
     */
    public function get(FunctionalTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');

        $url = $I->generateUrl('api.category.get');

        $I->sendGET($url);

        $I->seeResponseCodeIs(HttpCode::OK);

        $I->seeResponseIsJson();

        $I->seeResponseContainsJson(['success' => true, 'data' => [[]]]);

        $jsonPath = '$.data..id';
        $I->seeResponseJsonMatchesJsonPath($jsonPath);
        $ids = $I->grabDataFromResponseByJsonPath($jsonPath);

        return $ids[array_rand($ids)];
    }
}
