<?php declare(strict_types = 1);

use Codeception\Util\HttpCode;

/**
 * Class PostCest
 */
class PostCest
{
    const POST_TITLE = 'post title';

    const POST_UPDATE_TITLE = 'post update title';

    const POST_TEXT = 'post text';

    const POST_UPDATE_TEXT = 'post update text';

    /**
     * @param FunctionalTester $I
     * @return null|int
     */
    public function post(FunctionalTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');

        $categoryId = (new CategoriesCest())->get($I);

        $url = $I->generateUrl('api.post.post');

        $I->sendPOST($url, [
            'title'      => self::POST_TITLE,
            'text'       => self::POST_TEXT,
            'categoryId' => $categoryId,
        ]);

        $I->seeResponseCodeIs(HttpCode::OK);

        $I->seeResponseIsJson();

        $I->seeResponseContainsJson([
            'success' => true,
            'data'    => [
                'title'    => self::POST_TITLE,
                'text'     => self::POST_TEXT,
                'category' => [
                    'id' => $categoryId,
                ],
            ],
        ]);

        $I->canSeeInRepository(\ApiBundle\Entity\Post::class, [
            'title'    => self::POST_TITLE,
            'text'     => self::POST_TEXT,
            'category' => $categoryId,
        ]);

        $jsonPath = '$.data.id';
        $I->seeResponseJsonMatchesJsonPath($jsonPath);
        $id = $I->grabDataFromResponseByJsonPath($jsonPath);

        return $id[0] ?? null;
    }

    /**
     * @param FunctionalTester $I
     */
    public function get(FunctionalTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');

        $url = $I->generateUrl('api.post.get');

        $I->sendGET($url);

        $I->seeResponseCodeIs(HttpCode::OK);

        $I->seeResponseIsJson();

        $I->seeResponseContainsJson([
            'success' => true,
            'data'    => [[]],
        ]);

        $jsonPath = '$.data..id';
        $I->seeResponseJsonMatchesJsonPath($jsonPath);
        $ids = $I->grabDataFromResponseByJsonPath($jsonPath);

        return $ids[array_rand($ids)];
    }

    /**
     * @param FunctionalTester $I
     * @param int|null         $postId
     * @param int|null         $categoryId
     */
    public function put(FunctionalTester $I, int $postId = null, int $categoryId = null)
    {
        $postId = $postId ?? $this->post($I);
        $categoryId = $categoryId ?? (new CategoriesCest())->get($I);

        $I->haveHttpHeader('Content-Type', 'application/json');

        $url = $I->generateUrl('api.post.update', [
            'id' => $postId,
        ]);

        $I->sendPUT($url, [
            'title'      => self::POST_UPDATE_TITLE,
            'text'       => self::POST_UPDATE_TEXT,
            'categoryId' => $categoryId,
        ]);

        $I->seeResponseCodeIs(HttpCode::OK);

        $I->seeResponseIsJson();

        $I->seeResponseContainsJson([
            'success' => true,
            'data'    => [
                'title'    => self::POST_UPDATE_TITLE,
                'text'     => self::POST_UPDATE_TEXT,
                'category' => [
                    'id' => $categoryId,
                ],
            ],
        ]);

        $I->canSeeInRepository(\ApiBundle\Entity\Post::class, [
            'title'    => self::POST_UPDATE_TITLE,
            'text'     => self::POST_UPDATE_TEXT,
            'category' => $categoryId,
        ]);
    }

    /**
     * @param FunctionalTester $I
     */
    public function delete(FunctionalTester $I)
    {
        $postId = $this->post($I);

        $I->haveHttpHeader('Content-Type', 'application/json');

        $url = $I->generateUrl('api.post.delete', [
            'id' => $postId,
        ]);

        $I->sendDELETE($url);

        $I->seeResponseCodeIs(HttpCode::OK);

        $I->seeResponseIsJson();

        $I->dontSeeInRepository(\ApiBundle\Entity\Post::class, [
            'id'    => $postId,
            'title' => self::POST_TITLE,
            'text'  => self::POST_TEXT,
        ]);
    }
}
